<?php

namespace ContextualCode\EzPlatformPreviewSiteAccessMatcherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle extends Bundle
{
    protected $name = 'ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle';
}

