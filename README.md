# eZ Platform Preview SiteAccess Matcher Bundle
eZ Platform bundle which sets correct default siteaccess in admin preview.

## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/ezplatform-preview-siteaccess-matcher-bundle
```
- Enable this bundle in `app/AppKernel.php` file by adding next line in `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\EzPlatformPreviewSiteAccessMatcherBundle\ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle()
        );
```

And then add this to the bottom of your `app/config/routing.yml`:

```yml
contextual_code_ezplatform_preview_siteaccess_matcher:
    resource: "@ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle/Resources/config/routing.yml"
```
